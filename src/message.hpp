#pragma once
/*! \file */

#include <cstdint>
#include <iostream>

#include <ns3/nstime.h>
#include <ns3/buffer.h>

/*!
 * \brief The Message struct represents a single message stored at a node.
 *
 * A message is a block of data identified by an Id. On real world this Id
 * would be a hash of the data, but on simulation it is just a random value
 * as the data is also random and is not really stored in memory. The size
 * member represents the actual data.
 */
struct Message {

    /*!
     * \brief Construct a new uniq message.
     * \note createTime is **not** set on construction, it must be set later.
     */
    Message();

    /*!
     * \brief An uniq identifier for the message.
     *
     * This Id is random and collision probability exists, but is too low.
     * For all practical purposes, if two messages have identical Ids,
     * they are identical.
     */
    unsigned id;

    /*!
     * \brief The size of the data contained on the message.
     *
     * This is random and follows an exponential distribution.
     * \see Config::packetSizeParam
     */
    unsigned size;

    /*!
     * \brief Time when the message was created.
     *
     * This time is with reference of the clock where the message is currently
     * stored. The same message at different nodes will have different values
     * for it, but they will represent the same point in time.
     */
    ns3::Time createTime;

    /*!
     * \brief Computes the size it will take when serialized.
     * \return The number of bytes that the function serialize() will write.
     */
    uint32_t serializedSize() const;

    /*!
     * \brief Serializes the message into a buffer.
     * It will write a sequence of zeros to represent the message data.
     * \param buffer An iterator to where data will be written to.
     */
    void serialize(ns3::Buffer::Iterator buffer) const;

    /*!
     * \brief Deserializes the message from a buffer.
     * \param buffer An iterator to where data will be read from.
     * \return A new message with the same characteristics of
     *         the serialized one.
     */
    static Message deserialize(ns3::Buffer::Iterator buffer);

    /*!
     * \brief Operator to provide an order to a set of messages.
     * This is required for binary search and for `std::set`.
     * \param m Another message
     * \return The result of the comparison by Id.
     */
    bool operator<(const Message& m) const { return id < m.id; }

};

/*!
 * \brief Outputs the message Id in hex notation.
 * \param os An output stream.
 * \param m A message
 * \return The output stream itself.
 */
std::ostream& operator<<(std::ostream& os, const Message& m);
