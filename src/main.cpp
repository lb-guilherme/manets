/*! \file */

#include "world.hpp"
#include "config.hpp"

#include <random>
#include <iomanip>

#include <ns3/simulator.h>
#include <ns3/config.h>
#include <ns3/integer.h>
#include <ns3/log.h>

/*!
 * \brief A random device used everywhere.
 * It is also used to seed the internal ns3 random system.
 *
 * On Linux, this is truly random based on hardware entropy.
 */
std::random_device rnd;

/*!
 * \brief A random uniform distribution of 32-bit values.
 */
std::uniform_int_distribution<uint32_t> rnd32 =
    std::uniform_int_distribution<uint32_t>(0, 4294967295U);

/*!
 * \brief A callback to specify how a time reference should be printed on logs.
 *
 * It outputs a kernel-like time in seconds and with 4 decimal places.
 * Example: `[    8.4567] Log message here`
 * \param os The output stream.
 */
void logTimePrinter(std::ostream& os) {
    os << "[";
    os << std::setw(10) << std::fixed <<  std::setprecision(4);
    os << ns3::Simulator::Now().GetSeconds();
    os << std::setw(0);
    os << "]";
}

/*!
 * \brief The main function
 * \param argc Number of command line arguments.
 * \param argv Zero teminated array of these commands.
 * \return The exit code of the process. Always zero.
 */
int main(int argc, char *argv[]) {
    ns3::Config::SetGlobal("RngSeed", ns3::IntegerValue(rnd32(rnd)));

    Config::parse(argc, argv);
    Config::check();

    World::setup();

    ns3::LogSetTimePrinter(logTimePrinter);

    NS_LOG_UNCOND("Simulation with " << World::nodeCount() << " nodes.");

    World::run();

    return 0;
}

