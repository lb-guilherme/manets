#pragma once
/*! \file */

#include <string>

/*!
 * \brief The Config struct holds parameters of the simulation.
 *
 * They all have a default value and are set from the command line.
 */
struct Config {
    
    /*!
     * \brief Reads the parameters from the command line.
     * \see main()
     * \param argc The number of arguments.
     * \param argv An array of the arguments.
     */
    static void parse(int argc, char* argv[]);

    /*!
     * \brief Does some sanity tests on the arguments.
     * If something wrong is detected, the exception
     * `std::invalid_argument` is thrown.
     */
    static void check();

    /*!
     * \brief The number of nodes to simulate.
     * Time of simulation is highly dependent on this value, don't
     * use something too high.
     */
    static unsigned nodeCount;

    /*!
     * \brief The model of mobility.
     * Currently the only supported value for this parameter is `"uniform"`.
     *
     * \todo Use other mobility models.
     */
    static std::string mobility;

    // Values supported: `"random"; "energy"; "connectivity"; "mobility"; "signal"`
    //TODO: Make doc
    static std::string clusterHeadPolicy;

    /*!
     * \brief The area of simulation in meters.
     * The meaning of this parameter depends on the mobility model.
     *
     * - For `uniform` mobility: This value is the side of a square.
     */
    static unsigned area;

    //TODO: Make doc
    static unsigned nodeSpeed;

    /*!
     * \brief The duration of each cycle in milliseconds.
     *
     * This is the time each node stays connected to a single cluster head.
     * Too long means all messages will be exchanged and they will stay
     * idle for a lot of time. Too short means that not all messages will be
     * sent in time.
     */
    static unsigned cycleDuration;

    /*!
     * \brief The amount of messages on each node at time zero.
     * Use this to start the simulation with a given load.
     */
    static unsigned messageLoad;

    //TODO: Make doc
    // When set to zero, doesn't test convergence (default).
    static unsigned maxLoad;

    //TODO: Make doc
    static double erlang;
    static unsigned bandwidth;

    /*!
     * \brief Total duration of the simulation in milliseconds.
     */
    static unsigned duration;

    /*!
     * \brief Duration of the initial period in which all nodes comes to existence.
     *
     * The birth time of each node will be uniformly random in `[0, birthPeriod]`.
     * Set to zero to force all nodes to born at time zero.
     */
    static unsigned birthPeriod;

    /*!
     * \brief Parameter of the exponential distribution of packet sizes.
     *
     * The size of each packet is given by `std::exponential_distribution`
     * with the parameter `1.0/packetSizeParam`.
     * \see Message::Message()
     */
    static unsigned packetSizeParam;

     /*!
     * \brief Defines the how much power the wifi device will require.
     *
     * Estimative:
     * <pre>
     * +--------+-------------+
     * | Factor | Time for 2J |
     * +--------+-------------+
     * |    1   |   670~690   |
     * |    2   |   340~360   |
     * |    5   |   140~160   |
     * |   10   |    70~80    |
     * |   15   |    50~60    |
     * +--------+-------------+
     * </pre>
     */
    static double energyUsageFactor;

    /*!
     * \brief Specifies the minimal amount of battery for a node (joules).
     * Distribution is uniform.
     */
    static double batteryMin;

    /*!
     * \brief Specifies the maximal amount of battery for a node (joules).
     * Distribution is uniform.
     */
    static double batteryMax;

    /*!
     * \brief Path for the report file.
     *
     * Change this value to run the simulation multiple times and keep
     * results of all runs. Useful for postprocessing the data.
     */
    static std::string reportFile;

};

