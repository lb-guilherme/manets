#pragma once

#include <fstream>

class Stats
{
public:

    static void setup();

    static void report();

    static unsigned messageCount;
    static unsigned controlCount;

private:

    static std::fstream file;

};

