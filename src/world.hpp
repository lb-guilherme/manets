#pragma once

#include "node.hpp"

#include <string>
#include <vector>
#include <memory>
#include <fstream>

#include <ns3/mobility-helper.h>

namespace ns3 {
    class YansWifiChannel;
}

class World {
    // Allows Stats to query the node list
    friend class Stats;

public:

    static void setup();
    static void run();

    static unsigned nodeCount();
    static double now();
    
    static ns3::Ptr<ns3::YansWifiChannel> wifiChannel;
    static ns3::MobilityHelper mobilityHelper;
    
private:

    static void setupWifiChannel();
    static void setupMobility();

    static std::vector<std::unique_ptr<Node>> nodes;

};
