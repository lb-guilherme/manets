#pragma once

#include <ns3/header.h>
#include <ns3/ipv4-address.h>
#include <ns3/nstime.h>

namespace ns3 {
    class TypeId;
}

/*!
 * \brief The PacketHeader class describes a header attached to every packet
 *
 * Its main purpose is to give a type to each packet.
 */
class PacketHeader : public ns3::Header {
public:

    /*!
     * \brief Computes and returns meta information about this class.
     */
    static ns3::TypeId GetTypeId();

    /*!
     * \brief Same as GetTypeId(), but with virtual dispatch.
     */
    virtual ns3::TypeId GetInstanceTypeId() const;

    /*!
     * \brief Does nothing. This is required by the base class.
     * \param os An output stream
     */
    virtual void Print(std::ostream& os) const;

    /*!
     * \brief Serializes the header into a buffer. This is used to write the header on the packet.
     * \param buffer An buffer iterator for output.
     */
    virtual void Serialize(ns3::Buffer::Iterator buffer) const;

    /*!
     * \brief Deserializes the header from a buffer. This is used to read the header from a packet.
     * \param buffer An buffer iterator for output.
     * \return The number of bytes read.
     */
    virtual uint32_t Deserialize(ns3::Buffer::Iterator buffer);

    /*!
     * \brief Computes how many bytes the Serialize function will read.
     * \return Size of the header in bytes.
     */
    virtual uint32_t GetSerializedSize() const;

    /*!
     * \brief Type of the control message.
     */
    enum Type {
        //! Broadcast packet sent repeatedly by the cluster head.
        //! Other nodes will receive it and get to know that the cluster exists.
        //! This is also how the node knows the signal strenght of the possible cluster head.
        ClusterHeadIdentification,

        //TODO: Make doc
        Announce,

        //! Sent from a node to its cluster head. Attached to the packet there should be
        //! a list of the messages this node have. The cluster head will request the ones
        //! he doesn't have and will send the ones he has but the node does not.
        JoinCluster,

        //! A resquest from someone (usually the cluster head) who wants a message.
        //! An answer in the form of a PacketHeader::Message should be sent to him.
        MessageRequest,

        //! This packet contains a single serialized message.
        Message
    };

    //! The type of control packet. See PacketHeader::Type.
    Type type;

    //! The IPv4 where this packet came from.
    ns3::Ipv4Address origin;

    //! The current time from origin's clock.
    ns3::Time originClock;

};
