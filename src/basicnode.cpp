/*! \file */

#include "node.hpp"
#include "world.hpp"
#include "config.hpp"
#include "utils.hpp"
#include "packetheader.hpp"
#include "message.hpp"
#include "wifitag.hpp"

#include <utility>
#include <random>
#include <algorithm>

#include <ns3/log.h>
#include <ns3/string.h>
#include <ns3/mobility-model.h>
#include <ns3/wifi-mac.h>
#include <ns3/wifi-net-device.h>
#include <ns3/nqos-wifi-mac-helper.h>
#include <ns3/wifi-phy.h>
#include <ns3/wifi-phy-standard.h>
#include <ns3/yans-wifi-helper.h>
#include <ns3/ipv4-address-generator.h>
#include <ns3/internet-stack-helper.h>
#include <ns3/udp-socket-factory.h>
#include <ns3/basic-energy-source.h>
#include <ns3/basic-energy-source-helper.h>
#include <ns3/wifi-radio-energy-model.h>
#include <ns3/wifi-radio-energy-model-helper.h>

extern std::random_device rnd;
extern std::uniform_int_distribution<uint32_t> rnd32;

BasicNode::BasicNode() {
    node = ns3::CreateObject<ns3::Node>();
    clockReference = ns3::MilliSeconds(rnd32(rnd));

    setupWifi();
    setupIp();
    setupMobility();
    setupEnergy();
    setupSockets();
}

BasicNode::~BasicNode() {
}

std::ostream& operator<<(std::ostream& os, const BasicNode& n) {
    os << "Node #" << n.node->GetId() << "\n"
       << "  Position: " << n.mobility->GetPosition() << "\n"
       << "  Energy:   " << n.battery->GetRemainingEnergy() << " J" << "\n"
       << "  Wifi MAC: " << n.wifiDevice->GetMac()->GetAddress() << "\n"
       << "  IP:       " << n.addr;
    return os;
}

ns3::Time BasicNode::clock() {
    return clockReference + ns3::Simulator::Now();
}

void BasicNode::adjustClock(ns3::Time delta) {
    clockReference += delta;
}

bool BasicNode::isAlive() {
    return battery->GetRemainingEnergy() > 0;
}

void BasicNode::setupWifi() {
    // Wifi: Physical Layer
    ns3::YansWifiPhyHelper wifiPhyHelper = ns3::YansWifiPhyHelper::Default();
    wifiPhyHelper.Set("RxGain", ns3::DoubleValue(-10));
    wifiPhyHelper.Set("TxGain", ns3::DoubleValue(1));
    wifiPhyHelper.SetChannel(World::wifiChannel);

    // Wifi: MAC Layer
    ns3::NqosWifiMacHelper wifiMacHelper = ns3::NqosWifiMacHelper::Default();
    wifiMacHelper.SetType("ns3::AdhocWifiMac");
    
    // Wifi
    ns3::WifiHelper wifiHelper;
    wifiHelper.SetStandard(ns3::WIFI_PHY_STANDARD_80211b);
    wifiHelper.SetRemoteStationManager("ns3::ConstantRateWifiManager",
                                       "DataMode", ns3::StringValue("DsssRate1Mbps"),
                                       "ControlMode", ns3::StringValue("DsssRate1Mbps")
                                       );

    // Wifi: Devices
    auto netDevice = wifiHelper.Install(wifiPhyHelper, wifiMacHelper, node).Get(0);
    wifiDevice = (ns3::WifiNetDevice*)ns3::GetPointer(netDevice);

    wifiDevice->GetPhy()->TraceConnectWithoutContext(
                "MonitorSnifferRx",
                ns3::MakeCallback(&BasicNode::wifiReceive, this));

}

void BasicNode::wifiReceive(ns3::Ptr<const ns3::Packet> packet, uint16_t channelFreqMhz,
                            uint16_t channelNumber, uint32_t rate, bool isShortPreamble,
                            double signalDbm, double noiseDbm) {
    ns3::Packet* pkt = const_cast<ns3::Packet*>(ns3::PeekPointer(packet));
    WifiTag tag;
    tag.signalDbm = signalDbm;
    pkt->AddPacketTag(tag);
}

void BasicNode::setupIp() {
    ns3::InternetStackHelper().Install(node);

    ipv4 = node->GetObject<ns3::Ipv4>();
    
    int32_t interface = ipv4->AddInterface(wifiDevice);
    ns3::Ipv4InterfaceAddress ipv4Addr(ns3::Ipv4AddressGenerator::NextAddress("255.0.0.0"), "255.0.0.0");
    ipv4->AddAddress(interface, ipv4Addr);
    ipv4->SetMetric(interface, 1);
    ipv4->SetUp(interface);

    addr = ipv4->GetAddress(1, 0).GetLocal();
}

void BasicNode::setupSockets() {
    auto recvSink = node->GetObject<ns3::UdpSocketFactory>()->CreateSocket();
    recvSink->Bind(ns3::InetSocketAddress(ns3::Ipv4Address::GetAny(), 6000));
    recvSink->SetRecvCallback(ns3::MakeCallback(&BasicNode::receivePacket, this));
}

void BasicNode::setupMobility() {
    World::mobilityHelper.Install(node);
    mobility = node->GetObject<ns3::MobilityModel>();
}

void BasicNode::setupEnergy() {
    std::uniform_real_distribution<double> initialEnergyGenerator(Config::batteryMin, Config::batteryMax);

    ns3::BasicEnergySourceHelper basicSourceHelper;
    basicSourceHelper.Set("BasicEnergySourceInitialEnergyJ",
                          ns3::DoubleValue(initialEnergyGenerator(rnd)));
    battery = (ns3::BasicEnergySource*)ns3::GetPointer(basicSourceHelper.Install(node).Get(0));

    ns3::WifiRadioEnergyModelHelper radioModelHelper;
    double f = Config::energyUsageFactor;
    radioModelHelper.Set("IdleCurrentA",      ns3::DoubleValue(f*0.000426));
    radioModelHelper.Set("CcaBusyCurrentA",   ns3::DoubleValue(f*0.000426));
    radioModelHelper.Set("TxCurrentA",        ns3::DoubleValue(f*0.0174));
    radioModelHelper.Set("RxCurrentA",        ns3::DoubleValue(f*0.0197));
    radioModelHelper.Set("SwitchingCurrentA", ns3::DoubleValue(f*0.000426));
    radioModelHelper.SetDepletionCallback(ns3::MakeCallback(&BasicNode::notifyBatteryEnded, this));
    auto model = radioModelHelper.Install(wifiDevice, battery).Get(0);
    energyModel = (ns3::WifiRadioEnergyModel*)ns3::GetPointer(model);

    // Error on pre-ns21.
    battery->SetAttribute("BasicEnergyLowBatteryThreshold", ns3::DoubleValue(0));
}

void BasicNode::resetBattery() {
    // This is required for pre-ns21 support.
    // From ns21 and on we can simply do:
    // battery->SetInitialEnergy(battery->GetInitialEnergy());
    // Although it is still ilegal per documentation.

    double initial = battery->GetInitialEnergy();
    battery->SetInitialEnergy(999999999); // Sets a high value and update it so everything
    battery->UpdateEnergySource();        // that should be consumed is consumed from there
    battery->SetInitialEnergy(initial);   // This call only works when the battery was not empty
    alreadyNotifiedBatteryEnd = false; // Notify again if battery dies
}

void BasicNode::receivePacket(ns3::Ptr<ns3::Socket> socket) {
    if (!isAlive()) return;

    auto packet = socket->Recv();
    handleReceivedPacket(packet);
}

void BasicNode::notifyBatteryEnded() {
    if (!alreadyNotifiedBatteryEnd) {
        alreadyNotifiedBatteryEnd = true;
        handleBatteryEnded();
    }
}

void BasicNode::sendPacket(ns3::Ipv4Address target, PacketHeader::Type type, ns3::Buffer buffer) {
    if (!isAlive()) return;

    auto sock = node->GetObject<ns3::UdpSocketFactory>()->CreateSocket();
    sock->SetAllowBroadcast(true);
    sock->Connect(ns3::InetSocketAddress(target, 6000));

    uint8_t* data = const_cast<uint8_t*>(buffer.PeekData());
    uint32_t size = buffer.GetSize();

    PacketHeader header;
    header.type = type;
    header.origin = ipv4->GetAddress(1, 0).GetLocal();
    header.originClock = clock();

    auto packet = ns3::Create<ns3::Packet>(data, size);
    packet->AddHeader(header);

    if (!sock->Send(packet)) {
        std::clog << "ERROR" << std::endl;
        std::clog << header.origin << std::endl;
        std::clog << target << std::endl;
    }
}

void BasicNode::sendMessage(ns3::Ipv4Address target, const Message& msg) {
    ns3::Buffer buffer;
    buffer.AddAtEnd(msg.serializedSize());
    msg.serialize(buffer.Begin());

    sendPacket(target, PacketHeader::Message, buffer);
}
