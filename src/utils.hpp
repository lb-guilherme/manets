#pragma once

#include "stats.hpp"
#include <random>
#include <ns3/simulator.h>

extern std::random_device rnd;

namespace {
template <typename F>
static void callFunctor(F functor) {
    functor();
}
}

template <typename F>
inline void scheduleNow(const F& functor) {
    ns3::Simulator::ScheduleNow(&callFunctor<F>, functor);
}

template <typename F>
inline void schedule(ns3::Time time, const F& functor) {
    ns3::Simulator::Schedule(time, &callFunctor<F>, functor);
}

template <typename F>
inline void scheduleMany(ns3::Time time, unsigned count, const F& functor) {
    if (count == 0) return;
    scheduleNow([=]{
        functor();
        schedule(time, [=]{
            scheduleMany(time, count-1, functor);
        });
    });
}

template <typename F>
inline void scheduleEvery(ns3::Time time, const F& functor) {
    scheduleNow([=]{
        functor();
    });
    schedule(time, [=]{
        functor();
        scheduleEvery(time, functor);
    });
}

template <typename F>
inline void scheduleEveryExp(std::exponential_distribution<double> rate, const F& functor) {
    scheduleNow([=]{
        functor();
    });
    auto time = ns3::Seconds(rate(rnd));
    schedule(time, [=]{
        functor();
        scheduleEveryExp(rate, functor);
    });
}
