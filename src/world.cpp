#include "world.hpp"
#include "config.hpp"
#include "utils.hpp"
#include "stats.hpp"
#include "message.hpp"

#include <memory>
#include <random>

#include <ns3/double.h>
#include <ns3/pointer.h>
#include <ns3/yans-wifi-helper.h>
#include <ns3/yans-wifi-channel.h>
#include <ns3/rectangle.h>

extern std::random_device rnd;

ns3::Ptr<ns3::YansWifiChannel> World::wifiChannel;
ns3::MobilityHelper World::mobilityHelper;
std::vector<std::unique_ptr<Node>> World::nodes;

void World::setup() {
    World::setupWifiChannel();
    World::setupMobility();

    // Create all nodes
    for (unsigned i = 0; i < Config::nodeCount; ++i) {
        std::unique_ptr<Node> ptr(new Node);
        nodes.push_back(std::move(ptr));
    }

    if (Config::erlang > 0 && Config::bandwidth > 0 && Config::packetSizeParam > 0) {
        std::exponential_distribution<double> rate(Config::erlang * Config::bandwidth * 1000 / Config::packetSizeParam);
        scheduleEveryExp(rate, [&]{
            if (Config::maxLoad > 0 && Stats::messageCount >= Config::maxLoad) return;
            std::uniform_int_distribution<uint32_t> num(0, nodes.size()-1);
            nodes[num(rnd)]->generateMessage();
        });
    }

    Stats::setup();
}

void World::run() {
    ns3::Simulator::Run();
    ns3::Simulator::Destroy();
}

unsigned World::nodeCount() {
    return nodes.size();
}

double World::now() {
    return ns3::Simulator::Now().GetSeconds();
}

void World::setupWifiChannel() {
    ns3::YansWifiChannelHelper wifiChannelHelper;
    wifiChannelHelper.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
    wifiChannelHelper.AddPropagationLoss("ns3::FriisPropagationLossModel",
                                         "SystemLoss", ns3::DoubleValue(10)
                                         );
    wifiChannel = wifiChannelHelper.Create();
}

void World::setupMobility() {
    if (Config::mobility == "uniform") {
        auto positionGenerator = ns3::CreateObject<ns3::UniformRandomVariable>();
        positionGenerator->SetAttribute("Min", ns3::DoubleValue(0));
        positionGenerator->SetAttribute("Max", ns3::DoubleValue(Config::area));

        auto positionAllocator = ns3::CreateObject<ns3::RandomRectanglePositionAllocator>();
        positionAllocator->SetX(positionGenerator);
        positionAllocator->SetY(positionGenerator);

        auto speedGenerator = ns3::CreateObject<ns3::UniformRandomVariable>();
        speedGenerator->SetAttribute("Min", ns3::DoubleValue(0.1));
        speedGenerator->SetAttribute("Max", ns3::DoubleValue(Config::nodeSpeed));

        auto pauseGenerator = ns3::CreateObject<ns3::ConstantRandomVariable>();
        pauseGenerator->SetAttribute("Constant", ns3::DoubleValue(2));

        mobilityHelper.SetPositionAllocator(positionAllocator);
        mobilityHelper.SetMobilityModel("ns3::RandomWaypointMobilityModel",
                                        "Speed", ns3::PointerValue(speedGenerator),
                                        "Pause", ns3::PointerValue(pauseGenerator),
                                        "PositionAllocator", ns3::PointerValue(positionAllocator)
                                        );
    }
}
