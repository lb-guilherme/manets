#pragma once
/*! \file */

#include "packetheader.hpp"

#include <iostream>
#include <map>

#include <ns3/ipv4-address.h>

class Message;

namespace ns3 {
    class Node;
    class Socket;
    class NetDevice;
    class WifiNetDevice;
    class BasicEnergySource;
    class MobilityModel;
    class Ipv4;
    class Packet;
    class Time;
    class WifiRadioEnergyModel;
}


/*!
 * \brief The BasicNode class provides a low level representation of a single node.
 *
 * BasicNode stores all information needed for a node to exist, but does not
 * control how it will behave. This behavior is left for a subclass to handle.
 *
 * Each node has:
 *  - A reference to the actual ns3::Node
 *  - The IPv4 internet stack
 *  - A WiFi network module
 *  - A simple battery
 *  - The mobility model
 *  - An internal clock
 *
 * \note BasicNode is an abstract class. It can't be used directly.
 */
class BasicNode {
    // Allows World to create new nodes
    friend class World;

    // Allows Stats to query node properties
    friend class Stats;

public:

    /*!
     * \brief Reads the current value of the internal clock attached to the node.
     *
     * This clock can be adjusted by the adjustClock() function.
     * It's initial value is randomized.
     * \return The current time on device
     */
    ns3::Time clock();

    /*!
     * \brief Changes the current reference of the clock based on a delta value.
     *
     * After this call, clock() value will be `clock()+delta`.
     * \param delta The value to add to the clock reference.
     */
    void adjustClock(ns3::Time delta);

    /*!
     * \brief Defines a way to print simple debug information about the node.
     * \param os The output stream.
     * \param n The BasicNode reference which will be print.
     * \return The parameter `os`.
     */
    friend std::ostream& operator<<(std::ostream& os, const BasicNode& n);

    /*!
     * \brief Check if this node is able to send or receive packets.
     * \return `false` means this node does not exist and should be ignored.
     */
    virtual bool isAlive();

protected:


    /*!
     * \brief Setups all attributes of the node.
     *
     * This creates the ns3::Node and all objects that should be attached to it.
     * The clock reference is also randomized here.
     */
    BasicNode();

    /*!
     * \brief Destructor.
     */
    virtual ~BasicNode();

    BasicNode(const BasicNode&) = delete; //!< It shall not be copyed.
    BasicNode(BasicNode&&) = delete; //!< It shall not be moved.

    /*!
     * \brief Sends a packet from this node to destination.
     *
     * The packet will be sent using the wifi net device. This means it
     * may or may not arrive at destination and that there will be some
     * propagation delay.
     *
     * \param target The IPv4 address of the target.
     * \param type The control header to include. See PacketHeader.
     * \param buffer Aditional data to append to the packet. This is optional.
     */
    void sendPacket(ns3::Ipv4Address target, PacketHeader::Type type, ns3::Buffer buffer=ns3::Buffer());

    /*!
     * \brief Sends a Message to destination.
     *
     * This is a helper function.
     * It will simply call sendPacket() with the type PacketHeader::Message
     * and append the serialized message to the buffer.
     *
     * \param target The IPv4 address of the target.
     * \param msg A message to be sent to the target.
     */
    void sendMessage(ns3::Ipv4Address target, const Message &msg);

    /*!
     * \brief Brings the battery back to its initial state.
     */
    void resetBattery();

    /*!
     * \brief A virtual function called to handle incoming packets.
     *
     * A subclass of BasicNode should implement some highlevel action to take here.
     *
     * The packet contains a WifiTag attached to it. This way it is possible
     * to read the signal strength when the packet was received.
     *
     * \param packet The received packet.
     */
    virtual void handleReceivedPacket(ns3::Ptr<ns3::Packet> packet) = 0;

    /*!
     * \brief A notification from the energy model that the battery is gone.
     */
    virtual void handleBatteryEnded() {}

    //! \brief The wrapped node from ns3.
    ns3::Ptr<ns3::Node> node;

    //! \brief The wifi device responsible for the physical layer.
    ns3::Ptr<ns3::WifiNetDevice> wifiDevice;

    //! \brief The IPv4 stack of this node.
    ns3::Ptr<ns3::Ipv4> ipv4;

    //! \brief The energy source of the node.
    ns3::Ptr<ns3::BasicEnergySource> battery;

    //! \brief The model of energy usage by the wifi device;
    ns3::Ptr<ns3::WifiRadioEnergyModel> energyModel;

    //! \brief The mobility model. This stores position and speed,
    //! as well as the method that changes them.
    ns3::Ptr<ns3::MobilityModel> mobility;

    //! \brief The address of the wifi interface.
    //! This comes from the ipv4 stack.
    ns3::Ipv4Address addr;

private:

    /*!
     * \brief Creates the wifi device attached to the node.
     *
     * The physical layer has a reception gain of -10dB and a transmission gain of 1dB.
     *
     * The MAC layer is an Ad-Hoc WiFi.
     *
     * The manager uses DsssRate of 1Mbps for both data and control.
     */
    void setupWifi();

    /*!
     * \brief Creates the Internet stack.
     *
     * The node gets an arbitrary ip from the system
     */
    void setupIp();

    /*!
     * \brief Creates a mobility model.
     *
     * The mobility model is defined at World::mobilityHelper.
     * This simply instantializes it.
     */
    void setupMobility();

    /*!
     * \brief Creates a power source to be used as battery.
     *
     * \todo The whole energy system is missing.
     */
    void setupEnergy();

    /*!
     * \brief Creates a listener for incoming packets.
     *
     * The callback function is receivePacket().
     */
    void setupSockets();

    /*!
     * \brief The callback function called whenever a packet is received.
     *
     * The job of this function is to read the packet from the socket and
     * send it to the handleReceivedPacket() function. No real decision-making
     * happens here.
     *
     * \param socket The socket thet received a packet.
     */
    void receivePacket(ns3::Ptr<ns3::Socket> socket);

    /*!
     * \brief Whenever a packet is received this function is called.
     *
     * This is designed for logging details from the physical wifi channel,
     * but this is used to modify the incoming packet to add a WifiTag to it.
     * This way information can be saved and later queried by a subclass.
     *
     * \param packet The received packet.
     * \param channelFreqMhz The frequency of the channel.
     * \param channelNumber The number of this channel.
     * \param rate The data rate.
     * \param isShortPreamble Unknown.
     * \param signalDbm The signal strength.
     * \param noiseDbm The noise strength.
     */
    void wifiReceive(ns3::Ptr<const ns3::Packet> packet, uint16_t channelFreqMhz,
                     uint16_t channelNumber, uint32_t rate, bool isShortPreamble,
                     double signalDbm, double noiseDbm);

    /*!
     * \brief A notification from the energy model that the battery is gone.
     *
     * This will be called several times by the ns3 system.
     */
    void notifyBatteryEnded();

    /*!
     * \brief The reference value used by the clock.
     *
     * The current clock is calculated by adding the simulation time
     * to this value. The clock can be adjusted by changing this.
     */
    ns3::Time clockReference;

    /*!
     * \brief The notification about battery end have already been handled.
     */
    bool alreadyNotifiedBatteryEnd = false;

};
