#include "packetheader.hpp"

#include <ns3/type-id.h>

ns3::TypeId PacketHeader::GetTypeId() {
    static ns3::TypeId tid = ns3::TypeId("PacketHeader")
            .SetParent<ns3::Header>()
            .AddConstructor<PacketHeader>();
    return tid;
}

ns3::TypeId PacketHeader::GetInstanceTypeId() const {
    return GetTypeId();
}

void PacketHeader::Print(std::ostream& os) const {
    (void)os;
}

void PacketHeader::Serialize(ns3::Buffer::Iterator buffer) const {
    buffer.WriteU8((uint8_t)type);
    buffer.WriteHtonU32(origin.Get());
    buffer.WriteHtonU64(originClock.GetMilliSeconds());
}

uint32_t PacketHeader::Deserialize(ns3::Buffer::Iterator buffer) {
    type = (Type)buffer.ReadU8();
    origin.Set(buffer.ReadNtohU32());
    originClock = ns3::MilliSeconds(buffer.ReadNtohU64());
    return 14;
}

uint32_t PacketHeader::GetSerializedSize() const {
    return 14;
}
