#pragma once

#include <ns3/tag.h>

class WifiTag : public ns3::Tag {
public:

    static ns3::TypeId GetTypeId();
    virtual ns3::TypeId GetInstanceTypeId() const;

    virtual void Print(std::ostream& os) const;
    virtual void Serialize(ns3::TagBuffer i) const;
    virtual void Deserialize(ns3::TagBuffer i);
    virtual uint32_t GetSerializedSize() const;

    double signalDbm;

};
