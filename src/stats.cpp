#include "stats.hpp"
#include "config.hpp"
#include "world.hpp"
#include "utils.hpp"
#include "message.hpp"
#include "node.hpp"
#include <ns3/basic-energy-source.h>

std::fstream Stats::file;
unsigned Stats::messageCount;
unsigned Stats::controlCount;

void Stats::setup() {
    file.open(Config::reportFile, std::ios_base::out);
    messageCount = 0;
    controlCount = 0;
    scheduleEvery(ns3::MilliSeconds(100), &Stats::report);
}

void Stats::report() {
    unsigned completeNodes = 0;
    unsigned deadNodes = 0;
    unsigned bornNodes = 0;
    unsigned totalMessageCount = 0;
    double totalEnergy = 0;
    for (auto& n : World::nodes) {
        totalMessageCount += n->messageBag.size();
        if (n->messageBag.size() == messageCount) {
            ++completeNodes;
        }
        if (n->hasBorn()) {
            bornNodes += 1;
            totalEnergy += std::max(0.0, n->battery->GetRemainingEnergy());
            if (!n->isAlive())
                deadNodes += 1;
        }
        else {
            totalEnergy += 1;
        }
    }

    double averageMessageCount = (double)totalMessageCount / World::nodeCount();

    file
        << World::now() << " "        // Current time (seconds)
        << World::nodeCount() << " "  // Number of nodes
        << deadNodes << " "           // Number of nodes with zero battery
        << totalEnergy << " "         // Total amount of energy on the system
        << averageMessageCount << " " // Average number of messages per node
        << controlCount << " "        // Number of control packets
        << messageCount << " "        // Total number of messages
        << std::endl;

    if (bornNodes == World::nodeCount() &&
            (messageCount*World::nodeCount() == totalMessageCount || totalEnergy <= 0)) {
        // We'll stop the simulation. But first print some statistics.

        std::clog << std::endl;
        std::clog << "End of Simulation." << std::endl;
        std::clog << std::endl;
        for (auto& n : World::nodes) {
            std::clog << n->addr << " \t";
            std::clog << "M:" << n->messageBag.size() << "\t";
            std::clog << "B:" << n->battery->GetRemainingEnergy() << "\t";
            std::clog << std::endl;
        }

        ns3::Simulator::Stop();
    }
}
