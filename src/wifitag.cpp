#include "wifitag.hpp"

ns3::TypeId WifiTag::GetTypeId() {
    static ns3::TypeId tid = ns3::TypeId("WifiTag")
            .SetParent<ns3::Tag>()
            .AddConstructor<WifiTag>();
    return tid;
}

ns3::TypeId WifiTag::GetInstanceTypeId() const {
    return GetTypeId();
}

void WifiTag::Print(std::ostream& os) const {
    (void)os;
}

void WifiTag::Serialize(ns3::TagBuffer i) const {
    i.WriteDouble(signalDbm);
}

void WifiTag::Deserialize(ns3::TagBuffer i) {
    signalDbm = i.ReadDouble();
}

uint32_t WifiTag::GetSerializedSize() const {
    return sizeof(*this);
}
