#pragma once

#include "basicnode.hpp"
#include "wifitag.hpp"

class Node : public BasicNode
{
    // Allows World to create new nodes
    friend class World;

    // Allows Stats to query node properties
    friend class Stats;

public:

    void generateMessage();

protected:

    Node();

    void prepareForCycleBegin();
    void beginCycle();
    bool willBeClusterHead();

    uint16_t weightToBeClusterHead();
    void sendAnnounceInformation();

    bool hasBorn();
    virtual bool isAlive();

    virtual void handleReceivedPacket(ns3::Ptr<ns3::Packet> packet);
    virtual void handleBatteryEnded();

    void handleClusterHeadIdentification(PacketHeader header, WifiTag tag);
    void handleAnnounceInformation(PacketHeader header, ns3::Buffer& buffer);
    void handleJoinCluster(PacketHeader header, ns3::Buffer& buffer);
    void handleMessageRequest(PacketHeader header, ns3::Buffer& buffer);
    void handleMessage(PacketHeader header, ns3::Buffer& buffer);

private:

    std::set<Message> messageBag;
    std::map<ns3::Ipv4Address, std::set<uint32_t>> neighborsMessages;
    std::map<ns3::Ipv4Address, uint16_t> knownNeighbors;
    std::map<ns3::Ipv4Address, double> knownClusterHeads;
    ns3::Ipv4Address clusterHeadAddr;
    ns3::Time birthTime;

    enum {
        WaitCycle,
        ClusterHead,
        ReadyForCycle
    } mode;

};
