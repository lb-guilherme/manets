#include "node.hpp"
#include "config.hpp"
#include "utils.hpp"
#include "message.hpp"
#include "stats.hpp"
#include "world.hpp"

#include <random>
#include <cmath>
#include <cassert>
#include <iomanip>
#include <iostream>

#include <ns3/log.h>
#include <ns3/packet.h>
#include <ns3/wifi-net-device.h>
#include <ns3/wifi-phy.h>
#include <ns3/basic-energy-source.h>
#include <ns3/mobility-model.h>
#include <ns3/ipv4.h>

NS_LOG_COMPONENT_DEFINE("<Node>");

extern std::random_device rnd;

Node::Node() {
    birthTime = ns3::MilliSeconds(std::uniform_int_distribution<unsigned>(0, Config::birthPeriod)(rnd));

    schedule(birthTime, [&]{
        NS_LOG_INFO(addr << " have born.");
        resetBattery();

        for (unsigned i = 0; i < Config::messageLoad; ++i)
            generateMessage();

        prepareForCycleBegin();
    });
}

void Node::generateMessage() {
    if (!isAlive()) return;

    Message m;
    m.createTime = clock();
    messageBag.insert(m);
    NS_LOG_INFO(addr << " generates a new message " << m << " with size " << m.size << ".");
    ++Stats::messageCount;
}

void Node::prepareForCycleBegin() {
    if (!isAlive()) return;

    neighborsMessages.clear();
    knownClusterHeads.clear();

    uint64_t time = clock().GetMilliSeconds() % Config::cycleDuration;
    time = Config::cycleDuration - time;

    if (willBeClusterHead()) {
        NS_LOG_INFO(addr << " will be cluster head.");
        mode = ClusterHead;
        // Broadcast to everybody that we will be a Cluster Head
        int until = time + Config::cycleDuration;
        int times = std::max(until/500, 1);
        scheduleMany(ns3::MilliSeconds(500), times, [&]{
            sendPacket("255.255.255.255", PacketHeader::ClusterHeadIdentification, ns3::Buffer());
        });
    }
    else {
        mode = WaitCycle;
        // Give us some time to wait for cluster head identifications
        schedule(ns3::MilliSeconds(time), [&]{
            beginCycle();
        });
    }

    sendAnnounceInformation();
    knownNeighbors.clear();

    // Start preparation for the next cycle 10% before it begins
    schedule(ns3::MilliSeconds(time + Config::cycleDuration * 0.9), [&]{
        prepareForCycleBegin();
    });
}

bool Node::willBeClusterHead() {
    double probability;

    if (Config::clusterHeadPolicy == "random") {
        probability = 0.25;
    }
    else {

        // Compute this node's weight and the total weight of known nodes.
        uint16_t thisWeight = weightToBeClusterHead();
        uint32_t totalWeight = thisWeight;
        for (const auto& pair : knownNeighbors) {
            totalWeight += pair.second;
        }

        if (Config::clusterHeadPolicy == "connectivity")
            NS_LOG_INFO(thisWeight);

        // Give a chance to any node to be cluster head, but priorize higher weights
        // If there are no neighbors with weight, do nothing
        if (totalWeight == 0)
            probability = 0;
        else
            probability = (double)thisWeight / totalWeight;
    }

    NS_LOG_INFO(probability);

    return std::bernoulli_distribution(probability)(rnd);
}

bool Node::hasBorn() {
    return ns3::Simulator::Now() >= birthTime;
}

bool Node::isAlive() {
    return BasicNode::isAlive() && hasBorn();
}

uint16_t Node::weightToBeClusterHead() {
    if (Config::clusterHeadPolicy == "random")
        return 1;

    if (Config::clusterHeadPolicy == "energy")
        return battery->GetEnergyFraction()*65000;

    if (Config::clusterHeadPolicy == "connectivity")
        return knownNeighbors.size();

    if (Config::clusterHeadPolicy == "mobility")
        return hypot(mobility->GetVelocity().x, mobility->GetVelocity().y)*1000;

    assert(false);
}

void Node::sendAnnounceInformation() {
    if (!isAlive() || Config::clusterHeadPolicy == "random") return;

    // Sends announce packet to its neighbors for next cycle cluster election
    uint16_t weight = weightToBeClusterHead();

    ns3::Buffer buffer;
    buffer.AddAtEnd(2);
    buffer.Begin().WriteU16(weight);

    NS_LOG_INFO(addr << " broadcasts announce packet with weight " << weight << ".");
    sendPacket("255.255.255.255", PacketHeader::Announce, buffer);
}
void Node::beginCycle() {
    if (!isAlive()) return;

    mode = ReadyForCycle;
    NS_LOG_INFO(addr << " begins its cycle with " << knownClusterHeads.size() << " cluster heads in range.");

    if (knownClusterHeads.size() > 0) {
        // Join the cluster head with better signal
        double currentBest = 0;
        for (const auto& pair : knownClusterHeads) {
            if (pair.second < currentBest) { // lower is better
                currentBest = pair.second;
                clusterHeadAddr = pair.first;
            }
        }

        ns3::Buffer buffer;
        buffer.AddAtEnd(messageBag.size()*4);
        auto it = buffer.Begin();
        for (const Message& m : messageBag) {
            it.WriteHtonU32(m.id);
        }

        sendPacket(clusterHeadAddr, PacketHeader::JoinCluster, buffer);
    }
}

void Node::handleBatteryEnded() {
    // can't die unless born.
    if (hasBorn())
        NS_LOG_INFO(addr << " is out of power.");
}

void Node::handleReceivedPacket(ns3::Ptr<ns3::Packet> packet) {
    PacketHeader header;
    packet->RemoveHeader(header);
    ns3::Time clockDelta = clock() - header.originClock;
    if (clockDelta < 0) adjustClock(clockDelta);

    WifiTag tag;
    packet->RemovePacketTag(tag);

    ns3::Buffer buffer;
    buffer.AddAtEnd(packet->GetSize());
    packet->CopyData(const_cast<uint8_t*>(buffer.PeekData()), packet->GetSize());

    switch (header.type) {
    case PacketHeader::ClusterHeadIdentification:
        handleClusterHeadIdentification(header, tag);
        ++Stats::controlCount;
        break;
    case PacketHeader::Announce:
        handleAnnounceInformation(header, buffer);
        ++Stats::controlCount;
        break;
    case PacketHeader::JoinCluster:
        handleJoinCluster(header, buffer);
        ++Stats::controlCount;
        break;
    case PacketHeader::MessageRequest:
        handleMessageRequest(header, buffer);
        ++Stats::controlCount;
        break;
    case PacketHeader::Message:
        handleMessage(header, buffer);
        break;
    }
}

void Node::handleClusterHeadIdentification(PacketHeader header, WifiTag tag) {
    // This is for when we are waiting to start the cycle
    if (mode != WaitCycle) return;

    // Add this cluster head to the list of known cluster heads.
    // When the cycle begins, this list will be checked by signal.
    if (knownClusterHeads.find(header.origin) == knownClusterHeads.end()) {
        NS_LOG_INFO(addr << " finds the cluster head " << header.origin << ".");
    }
    knownClusterHeads[header.origin] = tag.signalDbm;
}

void Node::handleAnnounceInformation(PacketHeader header, ns3::Buffer& buffer) {
    // Stores the information for cluster head selection policy.
    knownNeighbors[header.origin] = buffer.Begin().ReadU16();
}

void Node::handleJoinCluster(PacketHeader header, ns3::Buffer& buffer) {
    // This is for cluster heads only
    if (mode != ClusterHead) return;

    int messageCount = buffer.GetSize()/4;
    NS_LOG_INFO(addr << " accepts " << header.origin << " with " << messageCount << " messages.");

    // Create a entry on the table of neighbors and list there the message he has
    auto& set = neighborsMessages[header.origin];
    {
        auto it = buffer.Begin();
        for (int i = 0; i < messageCount; ++i)
            set.insert(it.ReadNtohU32());
    }

    // Select which message we don't have.
    std::set<uint32_t> wantedMessages;
    for (uint32_t id : set) {
        bool hasMessage = false;
        for (const Message& m : messageBag)
            if (m.id == id)
                hasMessage = true;

        if (!hasMessage)
            wantedMessages.insert(id);
    }

    // If there are any, request them.
    if (wantedMessages.size() > 0) {
        ns3::Buffer buffer;
        buffer.AddAtEnd(wantedMessages.size()*4);
        auto it = buffer.Begin();
        for (uint32_t id : wantedMessages) {
            it.WriteHtonU32(id);
        }

        sendPacket(header.origin, PacketHeader::MessageRequest, buffer);
    }

    // Send him all messages he doesn't have.
    int t = 1;
    for (const Message& m : messageBag) {
        if (set.find(m.id) == set.end()) {
            set.insert(m.id);
            // Can't send all messages at once. (why not?)
            schedule(ns3::MilliSeconds(100*t++), [=]{
                sendMessage(header.origin, m);
            });

        }
    }
}

void Node::handleMessageRequest(PacketHeader header, ns3::Buffer& buffer) {
    int messageCount = buffer.GetSize()/4;
    NS_LOG_INFO(addr << " was requested to send " << messageCount << " messages to " << header.origin << ".");

    // Check if we have the messages we are requested to send.
    // If so, send them to request origin.
    auto it = buffer.Begin();
    for (int i = 0; i < messageCount; ++i) {
        uint32_t wantedId = it.ReadNtohU32();
        bool haveIt = false;

        for (const Message& m : messageBag) {
            if (m.id == wantedId) {
                haveIt = true;
                sendMessage(header.origin, m);
                break;
            }
        }

        if (!haveIt) {
            NS_LOG_INFO(addr << " doesn't have the message " << wantedId << ".");
        }
    }
}

void Node::handleMessage(PacketHeader header, ns3::Buffer& buffer) {
    Message received = Message::deserialize(buffer.Begin());

    received.createTime += clock() - header.originClock;

    // Check if it is a duplicate
    for (const Message& m : messageBag)
        if (m.id == received.id) {
            NS_LOG_INFO(addr << " received the duplicated message " << received << " from " << header.origin << " .");
            return;
        }

    // Insert it
    messageBag.insert(received);

    // If we are cluster head, send this to everyone who doesn't have it
    if (mode == ClusterHead) {
        for (auto& pair : neighborsMessages) {
            if (pair.second.find(received.id) == pair.second.end()) {
                sendMessage(pair.first, received);
                pair.second.insert(received.id);
            }
        }
    }

    NS_LOG_INFO(addr << " received the message " << received << " from " << header.origin << " and now holds " << messageBag.size() << " messages.");
}
