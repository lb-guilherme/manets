/*! \file */

#include "config.hpp"

#include <algorithm>
#include <stdexcept>

#include <ns3/command-line.h>

unsigned Config::nodeCount = 5;
std::string Config::mobility = "uniform";
std::string Config::clusterHeadPolicy = "random";
unsigned Config::area = 50;
unsigned Config::nodeSpeed = 3;
unsigned Config::cycleDuration = 5000;
unsigned Config::messageLoad = 0;
unsigned Config::maxLoad = 0;
double Config::erlang = 0.1;
unsigned Config::bandwidth = 1000;
unsigned Config::duration = 120000;
unsigned Config::birthPeriod = 0;
unsigned Config::packetSizeParam = 1000;
double Config::energyUsageFactor = 2;
double Config::batteryMin = 0.3;
double Config::batteryMax = 2.5;
std::string Config::reportFile = "./report.txt";

void Config::parse(int argc, char* argv[]) {
    ns3::CommandLine cmd;

    cmd.AddValue("NodeCount",
                 "Number of nodes to simulate (units).",
                 nodeCount);

    cmd.AddValue("Mobility",
                 "Type of mobility model to use (string). Valid values: uniform",
                 mobility);

    cmd.AddValue("ClusterHeadPolicy",
                 "Type of policy to choose cluster head (string). Valid values: random, energy, connectivity, mobility, signal",
                 clusterHeadPolicy);

    cmd.AddValue("Area",
                 "Size of the area the nodes will be allowed to walk in (meters).",
                 area);

    cmd.AddValue("NodeSpeed",
                 "Maximum node speed (m/s).",
                 nodeSpeed);

    cmd.AddValue("CycleDuration",
                 "The duration of each cycle (milliseconds).",
                 cycleDuration);

    cmd.AddValue("MessageLoad",
                 "Number of messages to give to each node at time zero (units).",
                 messageLoad);

    cmd.AddValue("MaxLoad",
                 "Total of messages created for which we want the time for converging (units).",
                 maxLoad);

    cmd.AddValue("Erlang",
                 "Network load in erlangs.",
                 erlang);

    cmd.AddValue("Bandwidth",
                 "Connection bandwidth (kbps).",
                 bandwidth);

    cmd.AddValue("Duration",
                 "Total simulation time (milliseconds).",
                 duration);

    cmd.AddValue("BirthPeriod",
                 "Initial period where nodes are allowed to born (milliseconds).",
                 birthPeriod);

    cmd.AddValue("PacketSizeParam",
                 "The inverse of the lambda parameter for the exponential distribution of packet sizes (real). In other words, the average packet size (bits).",
                 packetSizeParam);

    cmd.AddValue("EnergyUsageFactor",
                 "A factor to multiply the default energy usage of a node (real).",
                 energyUsageFactor);

    cmd.AddValue("BatteryMin",
                 "The minimal amount of battery for a node (joules).",
                 batteryMin);

    cmd.AddValue("BatteryMax",
                 "The maximal amount of battery for a node (joules).",
                 batteryMax);

    cmd.AddValue("ReportFile",
                 "Path for the report file (string).",
                 reportFile);

    cmd.Parse(argc, argv);
}

void Config::check() {
    if (nodeCount < 2)
        throw std::invalid_argument("node count is too low");

    std::vector<std::string> validMobilityModels = {
        "uniform"
    };

    std::vector<std::string> validClusterHeadPolicy = {
        "random", "energy", "connectivity", "mobility", "signal"
    };

    if (std::find(validMobilityModels.begin(), validMobilityModels.end(), mobility) == validMobilityModels.end())
        throw std::invalid_argument("mobility has unknown value");

    if (std::find(validClusterHeadPolicy.begin(), validClusterHeadPolicy.end(), clusterHeadPolicy) == validClusterHeadPolicy.end())
        throw std::invalid_argument("cluster head policy has unknown value");
}

