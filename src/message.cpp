/*! \file */

#include "message.hpp"
#include "config.hpp"

#include <random>
#include <iomanip>
#include <set>

extern std::random_device rnd;
extern std::uniform_int_distribution<uint32_t> rnd32;

Message::Message() : id(rnd32(rnd)) {
    size = std::exponential_distribution<double>(1.0/Config::packetSizeParam)(rnd);

    // FIXME
    // Limit transmission over UDP Sockets on IPv4 is 65507 bytes
    // Algorithm should be able to fragment bigger messages over several packets
    if (size > 65000)
        size = 65000;
}

uint32_t Message::serializedSize() const {
    return 4 + 4 + 8 + size;
}

void Message::serialize(ns3::Buffer::Iterator buffer) const {
    buffer.WriteHtonU32(id);
    buffer.WriteHtonU32(size);
    buffer.WriteHtonU64(createTime.GetMilliSeconds());
    uint8_t* zeros = new uint8_t[size];
    memset(zeros, 0, size);
    buffer.Write(zeros, size);
    delete zeros;
}

Message Message::deserialize(ns3::Buffer::Iterator buffer) {
    Message m;
    m.id = buffer.ReadNtohU32();
    m.size = buffer.ReadNtohU32();
    m.createTime = ns3::MilliSeconds(buffer.ReadNtohU64());
    uint8_t* zeros = new uint8_t[m.size];
    buffer.Read(zeros, m.size);
    delete zeros;
    return m;
}

std::ostream& operator<<(std::ostream& os, const Message& m) {
    os << std::hex << std::setw(8) << std::setfill('0') << m.id;
    os << std::setw(0) << std::setfill(' ') << std::dec;
    os << " (" << m.size << "B)";
    return os;
}
