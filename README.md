![converage1.png](https://bitbucket.org/repo/7nn968/images/2477968237-converage1.png)

# Prerequisitos

Funcionará apenas no Linux. Você precisará de um compilador de c++, do make, do ruby e do gnuplot. Pode ser instalado assim:

    sudo apt-get install g++ make ruby gnuplot
    
Depois precisará instalar um módulo de estatística do ruby:

    sudo gem install distribution

# Usando o Simulador

Funciona apenas em ambientes Linux. Use o comando `make` para baixar e compilar o ns3, compilar o simulador e o executar:

    make

Se algo não funcionou, cheque os arquivos `*.log` criados durante a compilação.

Use `make pdfs` para baixar os tutoriais e referências disponíveis no site. E `make docs` para baixar os pdfs e construir a [documentação em doxygen](http://www.nsnam.org/docs/release/3.20/doxygen/index.html).

Tudo pode ser compilado com outra versão do ns3 usando a variável `NS3_VERSION`. Exemplo: `make NS3_VERSION=3.19`. Note entretanto que usar versões diferentes não é garantido que vá funcionar.

# Parâmetros de Simulação

Passe para o `make` alguma combinação dos seguites parâmetros para afetar a simuação. Por exemplo:

    make NodeCount=10
    
## Lista de Parâmetros:

 - **`NodeCount=`*`5`*:** Número de nodos para considerar na simulação.
 - **`Mobility=`*`uniform`*:** O modelo de mobilidade que será usado. Valores válidos:
    - `uniform`: Todos os nodos são posicionados uniformemente no espaço e têm velocidades também uniformes. Tendência é não haver variação na topologia com o passar do tempo.
 - **`Area=`*`50`*:** Tamanho da área na qual o nodo poderá se mover (em metros).
 - **`CycleDuration=`*`5000`*:** Duração de cada ciclo (em milissegundos).
 - **`MessageLoad=`*`0`*:** Número inicial de mensagens em cada nodo (em unidades).
 - **`MessageFrequency=`*`3000`*:** Frequência para gerar novas mensagens (em milissegundos).
 - **`Duration=`*`25000`*:** Duração da simulação (em milissegundos).
 
# Gráficos

Gere resultados de simulação usando `make reports`. A variável `COUNT` define a quantidade de testes por métrica, valores mais altos darão resultados mais precisos. Exemplo: `make reports COUNT=50`. Sugiro utilizar `-j` para realizar várias execuções em paralelo. Exemplo: `make reports -j5`.