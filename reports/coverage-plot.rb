require 'distribution'

$datadir = "data-coverage"
$confidency = 0.90

n = 99999
for policy in %w[random energy connectivity mobility]
  for nodes in (10..50).step(10)
    i = 0
    while true
      file = "#{$datadir}/report-#{policy}-#{nodes}nodes-run#{i}.txt"
      break unless File.exists? file
      i += 1
    end
    n = [n, i].min
  end
end

def value(policy, nodes, i)
  file = "#{$datadir}/report-#{policy}-#{nodes}nodes-run#{i}.txt"
  File.readlines(file).last.split(" ")[0].to_f
end

f =  Distribution::T.ptsub((1-$confidency)/2, n)
puts "n = #{n}"
puts "c = #{100*$confidency}%"
puts "f = #{f}"
for policy in %w[random energy connectivity mobility]
  result = [[0, 0, 0, 0]]
  for nodes in (10..50).step(10)
    values = (0...n).map {|i| value(policy, nodes, i) }
    
    m = values.reduce(:+) / n.to_f
    d = Math.sqrt(values.map {|v| (v-m)**2 }.reduce(:+) / n.to_f)

    delta = f * d / Math.sqrt(n)
    
    h = %w[random energy connectivity mobility].index(policy)
    nodes = nodes-1+h*0.5
    result << [nodes, [m - delta, 0].max, m, m + delta]
    
    p [policy, nodes, [m - delta, 0].max, m, m + delta]
  end
  File.write("#{$datadir}/#{policy}-result.txt", result.map {|e| e.join(" ")}.join("\n"))
end

system "gnuplot coverage.gp"

