
$datadir = "data-coverage"

system "rm -rf #{$datadir}"
system "mkdir #{$datadir}"

def run(nodes, policy, i)
  base = "#{$datadir}/report-#{policy}-#{nodes}nodes-run#{i}"
  file = "reports/#{base}.txt"
  log = "reports/#{base}.log"
  cmd = "cd .. && make"
  cmd << " NodeCount=#{nodes}"
  cmd << " ClusterHeadPolicy=#{policy}"
  cmd << " Area=10"
  cmd << " MessageLoad=5"
  cmd << " MaxLoad=#{5*nodes}"
  cmd << " Duration=5000000"
  cmd << " PacketSizeParam=8000"
  cmd << " EnergyUsageFactor=1"
  cmd << " BatteryMin=200"
  cmd << " BatteryMax=500"
  cmd << " ReportFile=#{file}"
  cmd << " >#{log} 2>&1"
  exit unless system cmd
  puts "[run %3d] %d nodes with policy '%s'" % [i, nodes, policy]
end

999.times do |i|
  threads = []
  for policy in %w[random energy connectivity mobility]
    for nodes in (10..50).step(10)
      threads << Thread.new(nodes, policy, i) {|nodes, policy, i| run(nodes, policy, i)}
    end
  end
  threads.map(&:join)
  system "ruby coverage-plot.rb"
end

