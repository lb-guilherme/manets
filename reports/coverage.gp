#set terminal pngcairo transparent truecolor enhanced fontscale 1.1 size 800,600
set terminal pngcairo truecolor enhanced fontscale 1.1 size 800,600
set output "coverage.png"
set xrange [0:60]
set boxwidth 0.4
set title "Tempo de convergência por número de nodos e\npolítica de escolha de cluster head\n(90% de confiança)"
set xlabel "Número de nodos"
set ylabel "Tempo até convergência completa (s)"
set key inside vertical
set key bottom right

plot \
\
"data-coverage/random-result.txt" using 1:3 with lines linecolor 1 notitle, \
"" using 1:2:2:4:4 with candlesticks linecolor 1 title "Escolha aleatória", \
\
"data-coverage/energy-result.txt" using 1:3 with lines linecolor 2 notitle, \
"" using 1:2:2:4:4 with candlesticks linecolor 2 title "Priorizar maior energia", \
\
"data-coverage/connectivity-result.txt" using 1:3 with lines linecolor 3 notitle, \
"" using 1:2:2:4:4 with candlesticks linecolor 3 title "Priorizar maior conectividade", \
\
"data-coverage/mobility-result.txt" using 1:3 with lines linecolor 4 notitle, \
"" using 1:2:2:4:4 with candlesticks linecolor 4 title "Priorizar maior mobilidade", \

