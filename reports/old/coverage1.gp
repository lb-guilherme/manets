set terminal pngcairo transparent truecolor enhanced fontscale 1.1 size 800,600
set output "reports/converage1.png"
set yrange [0:100]
set boxwidth 0.0000001
set style fill solid noborder
set title "Intervalo da taxa de alcance das mensagens\n(95% de confiança)"
set xlabel "Tempo de Simulação (segundos)"
set ylabel "Alcance das Mensagens (%)"
set key inside vertical
set key bottom right
set key samplen -1
plot "reports/data/coverage1-result.txt" using 1:2:2:3:3 every 105 with candlesticks title "20 nós em uma área de 500m²"
