
COUNT = 10
NUMBERS = $(shell seq 1 $(COUNT))
DATADIR = reports/data

reports: coverage1

coverage1-run%: main
	$(call run, \
		--NodeCount=20 \
		--Area=500 \
		--MessageLoad=10 \
		--MaxLoad=10 \
		--Duration=120000 \
		--BatteryMin=30 \
		--BatteryMax=80 \
	)

coverage1: $(addprefix coverage1-run,$(NUMBERS))
	$(call plot,coverage1,coverage,coverage)

###

define plot#(plotter.gp, data.txt, processor.rb)
	@echo "Processing  "$1" ... "
	@ruby reports/$3.rb \
	    $(addsuffix -$2.txt,$(addprefix $(DATADIR)/$1-run,$(NUMBERS))) \
	    $(DATADIR)/$1-
	@echo "Plotting    "$1" ... "
	@gnuplot reports/$1.gp 
endef

define run#(args)
	@echo "Running     "$@" ... ";
	@mkdir -p reports/data;
	@\
	LD_LIBRARY_PATH=$(NS3_DIR)/build \
	NS_LOG="<Node>=level_all|prefix_time" \
	./main $1 --Prefix=$(DATADIR)/$@- \
	> $(DATADIR)/$@.log \
	2>&1;
endef

