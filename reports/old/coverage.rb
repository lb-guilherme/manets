require 'json'
require 'distribution'

prefix = ARGV.pop
data = ARGV.map {|f| File.readlines(f).map{|line| line.split(" ").map(&:to_f) } }

result = []
n = data.size
f =  Distribution::T.ptsub(0.05/2, n)
data[0].map{|m|m[0]}.each_with_index do |time, i|
    values = data.map{|g|g[i][1]}

    m = values.reduce(:+) / n.to_f
    d = Math.sqrt(values.map {|v| (v-m)**2 }.reduce(:+) / n.to_f)

    delta = f * d / Math.sqrt(n)
    result << [time,
        m - delta,
        m + delta,
        m
    ]
end

File.write(prefix+"result.txt", result.map {|e| e.join(" ")}.join("\n"))
