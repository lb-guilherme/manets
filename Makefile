
NS3_VERSION = 3.21
NS3_BASEDIR = ns-allinone-$(NS3_VERSION)
NS3_DIR = $(NS3_BASEDIR)/ns-$(NS3_VERSION)
NS3_PACK = ns-allinone-$(NS3_VERSION).tar.bz2
NS3_LINK = https://www.nsnam.org/release/$(NS3_PACK)
NS3_DOCLINK = http://www.nsnam.org/docs/release/$(NS3_VERSION)

CXX = g++
COMPILE = $(CXX) -O3 -std=c++11 -Wall -Wextra -I$(NS3_DIR)/build -DNS3_LOG_ENABLE -Wno-unused-parameter

LIBS = $(wildcard $(NS3_DIR)/build/*.so)
OBJS = $(patsubst src/%.cpp, build/%.o, $(wildcard src/*.cpp))

LINKFLAGS = -L$(NS3_DIR)/build $(patsubst lib%.so, -l%, $(notdir $(LIBS)))

NodeCount = 5
Mobility = uniform
ClusterHeadPolicy = random
Area = 50
NodeSpeed = 3
CycleDuration = 5000
MessageLoad = 0
MaxLoad = 0
Erlang = 0.1
Bandwidth = 1000
Duration = 120000
BirthPeriod = 0
PacketSizeParam = 1000
EnergyUsageFactor = 2
BatteryMin = 0.3
BatteryMax = 2.5
ReportFile = ./report.txt

.PHONY: run
run: main
	@\
	LD_LIBRARY_PATH=$(NS3_DIR)/build \
	NS_LOG="<Node>=level_all|prefix_time" \
	./main \
	        --NodeCount=$(NodeCount) \
	        --Mobility=$(Mobility) \
	        --ClusterHeadPolicy=$(ClusterHeadPolicy) \
	        --Area=$(Area) \
	        --NodeSpeed=$(NodeSpeed) \
	        --CycleDuration=$(CycleDuration) \
	        --MessageLoad=$(MessageLoad) \
	        --MaxLoad=$(MaxLoad) \
	        --Erlang=$(Erlang) \
	        --Bandwidth=$(Bandwidth) \
	        --Duration=$(Duration) \
	        --BirthPeriod=$(BirthPeriod) \
	        --PacketSizeParam=$(PacketSizeParam) \
	        --EnergyUsageFactor=$(EnergyUsageFactor) \
	        --BatteryMin=$(BatteryMin) \
	        --BatteryMax=$(BatteryMax) \
	        --ReportFile=$(ReportFile)

.PHONY: help
help: main
	@LD_LIBRARY_PATH=$(NS3_DIR)/build ./main --PrintHelp

main: build-ns3 $(OBJS) $(LIBS)
	@echo "Linking     "$@" ..."
	@$(COMPILE) $(OBJS) -o $@ $(LINKFLAGS)

$(NS3_PACK):
	@echo "Downloading $(NS3_PACK) ... "
	@wget -q $(NS3_LINK)

$(NS3_BASEDIR): $(NS3_PACK)
	@echo "Unpacking   $(NS3_PACK) ... "
	@tar xjf $(NS3_PACK)

configure-ns3: $(NS3_BASEDIR)
	@echo "Configuring ns-$(NS3_VERSION) ... "
	@cd $(NS3_DIR) && python waf configure -d release > ../../configure.log 2>&1
	@touch configure-ns3
	
build-ns3: configure-ns3
	@echo "Building    ns-$(NS3_VERSION) ... "
	@cd $(NS3_DIR) && python waf build > ../../build.log 2>&1
	@touch build-ns3

docs-ns3: configure-ns3
	@echo "Generating  docs for ns-$(NS3_VERSION) ... "
	@echo "CREATE_SUBDIRS = YES" >> $(NS3_DIR)/doc/doxygen.conf
	@rm -rf ns-$(NS3_VERSION)-doc
	@cd $(NS3_DIR) && python waf --doxygen > ../../docs.log 2>&1
	@mv $(NS3_DIR)/doc/html ./ns-$(NS3_VERSION)-doc
	@touch docs-ns3
	
.PHONY:
docs: pdfs docs-ns3 doc

doc: Doxyfile src
	@doxygen

.PHONY: pdfs
pdfs: ns-$(NS3_VERSION)-tutorial.pdf
pdfs: ns-$(NS3_VERSION)-tutorial.pt-br.pdf
pdfs: ns-$(NS3_VERSION)-manual.pdf
pdfs: ns-$(NS3_VERSION)-model-library.pdf

ns-$(NS3_VERSION)-tutorial.pdf:
	@echo "Downloading "$@" ... "
	@wget -q $(NS3_DOCLINK)/tutorial/ns-3-tutorial.pdf -O $@

ns-$(NS3_VERSION)-tutorial.pt-br.pdf:
	@echo "Downloading "$@" ... "
	@wget -q $(NS3_DOCLINK)/tutorial-pt-br/ns-3-tutorial.pdf -O $@

ns-$(NS3_VERSION)-manual.pdf:
	@echo "Downloading "$@" ... "
	@wget -q $(NS3_DOCLINK)/manual/ns-3-manual.pdf -O $@

ns-$(NS3_VERSION)-model-library.pdf:
	@echo "Downloading "$@" ... "
	@wget -q $(NS3_DOCLINK)/models/ns-3-model-library.pdf -O $@

build/%.d: src/%.cpp build-ns3
	@mkdir -p $(dir $@)
	@$(COMPILE) -nostdinc -I$(NS3_DIR)/build -MF$@ -MG -MM -MT$@ -MT$(@:.d=.o) $<

-include $(patsubst src/%.cpp, build/%.d, $(wildcard src/*.cpp))

build/%.o: src/%.cpp build-ns3
	@echo "Compiling   "$@" ..."
	@mkdir -p $(dir $@)
	@$(COMPILE) -c $< -o $@

.PHONY: clean clean-ns3
clean-ns3: clean
	@rm -rf $(NS3_BASEDIR)
	@rm -f *.log configure-ns3 build-ns3

clean:
	@rm -rf build reports/data doc
	@rm -f main

